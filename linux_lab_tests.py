import ftplib
import io
import socket
import unittest
import subprocess
from datetime import datetime
from ftplib import FTP
from pprint import pprint

import dns.resolver

import requests

from adapters import ForcedIPHTTPAdapter

IP = '192.168.1.67'
OS_USER = 'user'
OS_PASS = '1234'

resolver = dns.resolver.Resolver(configure=False)
resolver.nameservers = [IP]


def vm1ssh(command, user='user'):
    print(command)
    exit_code = subprocess.call([f'ssh {user}@{IP} "{command}"'], shell=True, stdout=True, stderr=True)
    if exit_code != 0:
        raise ChildProcessError(f'Exit code: {exit_code}')
    return exit_code


def vm2ssh(command):
    exit_code = subprocess.call([f'ssh user@{IP} -p 2222 "{command}"'], shell=True, stdout=True, stderr=True)
    if exit_code != 0:
        raise ChildProcessError(f'Exit code: {exit_code}')
    return exit_code


class NetTests(unittest.TestCase):
    def test_ssh_vm1(self):
        vm1ssh('ip a')

    def test_ssh_vm2(self):
        vm2ssh('ip a')

    def check_port(self, number):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(5)
        res = sock.connect_ex((IP, number))
        sock.close()
        return res

    def test_firewall_smtp_open(self):
        self.assertEqual(self.check_port(25), 0, 'port closed')

    def test_firewall_imap_open(self):
        self.assertEqual(self.check_port(143), 0, 'port closed')

    def test_firewall_dns_open(self):
        self.assertEqual(self.check_port(53), 0, 'port closed')

    def test_firewall_mysql_closed(self):
        self.assertNotEqual(self.check_port(3306), 0, 'port open')


class DnsTests(unittest.TestCase):
    def test_a_resolving(self):
        domains = ('', 'srv01', 'srv02', 'mail', 'docs', 'info')
        for domain in domains:
            with self.subTest(domain):
                full_domain = 'lab.itiscloud.ru'

                if domain != '':
                    full_domain = f'{domain}.{full_domain}'

                print(resolver.query(full_domain, 'a', lifetime=10).response)

    def test_proxy_resolving(self):
        print(resolver.query('ya.ru', 'a', lifetime=10).response)


class FtpTests(unittest.TestCase):
    def test_anon(self):
        with FTP(IP) as ftp:
            with self.subTest('anon login'):
                print(ftp.login())  # анонимный вход
                ftp.cwd('/')

            with self.subTest('ls'):
                data = []
                ftp.dir(data.append)
                print(data)
                self.assertGreater(len(data), 0, 'files on ftp not found')

            with self.subTest('write'):
                with self.assertRaises(ftplib.error_perm):
                    ftp.storlines("STOR new_anon_file", datetime.now())

    def test_user(self):
        with FTP(IP, OS_USER, OS_PASS) as ftp:
            with self.subTest('login'):
                print(ftp.pwd())
                # ftp.cwd('/')

            filename = f'new_file_{datetime.now().microsecond}'
            print('filename', filename)

            with self.subTest('write'):
                f = io.BytesIO(bytes(str(datetime.now()), encoding='utf8'))
                print(ftp.storlines(f"STOR {filename}", f))

            with self.subTest('ls'):
                data = []
                print(ftp.dir(data.append))
                pprint(data)
                self.assertGreater(len(data), 0, 'files on ftp not found')


class SambaTests(unittest.TestCase):
    def test_distr_read(self):
        print(vm1ssh(f'smbclient --user {OS_USER} \\\\\\\\\\\\\\\\SRV02\\\\\\\\Distr {OS_PASS} -c "dir"'))

    # TODO write deny test:
    # touch aaa && smbclient --user {OS_USER} \\\\\\\\\\\\\\\\SRV02\\\\\\\\Distr {OS_PASS} -c "put aaa"

    def test_manuals_read_user(self):
        print(vm1ssh(f'smbclient --user {OS_USER} \\\\\\\\\\\\\\\\SRV02\\\\\\\\Manuals {OS_PASS} -c "dir"'))

    def test_manuals_read_bob(self):
        print(vm1ssh(f'smbclient --user bob \\\\\\\\\\\\\\\\SRV02\\\\\\\\Manuals {OS_PASS} -c "dir"'))

    # TODO write success test:
    # touch aaa && smbclient --user bob \\\\\SRV02\\Manuals {OS_PASS} -c "put aaa"

    def test_work_read_user(self):
        print(vm1ssh(f'smbclient --user {OS_USER} \\\\\\\\\\\\\\\\SRV02\\\\\\\\Work {OS_PASS} -c "dir"'))

    def test_work_cannot_read_user(self):
        with self.assertRaises(ChildProcessError):
            print(vm1ssh(f'smbclient --user bob \\\\\\\\\\\\\\\\SRV02\\\\\\\\Work {OS_PASS} -c "dir"'))


class WebTests(unittest.TestCase):
    def setUp(self):
        self.session = requests.Session()
        self.session.mount('http://docs.lab.itiscloud.ru', ForcedIPHTTPAdapter(dest_ip=IP))
        self.session.mount('http://info.lab.itiscloud.ru', ForcedIPHTTPAdapter(dest_ip=IP))

    def test_docs(self):
        response = self.session.get('http://docs.lab.itiscloud.ru/index.php/login')
        response.raise_for_status()
        self.assertTrue('Nextcloud' in response.text)

    def test_info(self):
        response = self.session.get('http://info.lab.itiscloud.ru/info.php', auth=('user', '1234'))
        response.raise_for_status()
        self.assertTrue('php' in response.text)
